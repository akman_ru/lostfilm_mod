// ==UserScript==
// @name         lostfilm.tv ad remover
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Remove Ads on lostfilm.tv
// @author       A.Kapitman
// @match        http://www.lostfilm.tv/
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    $('.aodd').remove();
})();