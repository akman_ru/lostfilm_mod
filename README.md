# LostFilm Mod 0.1

## Скрипт удаляет рекламу на lostfilm.tv

Для работы скрипта в браузере должно быть установлено расширение [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=ru) для Google Chrome или  
[Greasemonkey](https://addons.mozilla.org/ru/firefox/addon/greasemonkey/) для Mozilla Firefox. 

Ссылка для загрузки и обновления скрипта:
[https://bitbucket.org/akman_ru/lostfilm_mod/raw/2e96fa3d0da59e8aa4ecb8b2369c610b429c119b/lostfilm_mod.user.js](https://bitbucket.org/akman_ru/lostfilm_mod/raw/2e96fa3d0da59e8aa4ecb8b2369c610b429c119b/lostfilm_mod.user.js)

Скрипт работает **только** на странице ["lostfilm.tv"](http://lostfilm.tv)!
